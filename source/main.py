from modules.company import Company
from modules.input import Input
from modules.output import Output
import time
import sys
import logging as logger

logger.basicConfig(filename='log/filings-analyzer.log', level=logger.INFO, filemode='w', format='[%(levelname)s] %(module)s:%(funcName)s - %(message)s')

input_file_path = str(sys.argv[1])
output_file_path = str(sys.argv[2])

def main():
    input = Input(input_file_path)
    output = Output(output_file_path)

    input_generator = input.get_row()

    for index, input_item in input_generator:
        print("Processing [{}] {}".format(index, input_item[0]))
        try:
            company = Company(input_item[0], input_item[1:])
            output.update(company.get_result())
        except Exception as e:
            logger.exception("Exception when processing [{}] {}. Reason: {}", index, input_item[0], e)
            output.export()
    
    print("[DONE] Exporting to output file")
    output.export()
    


if __name__ == "__main__":
    with open("version") as f:
        version = f.read()

    print("Starting main program. Version is v{}".format(version))
    logger.info("Starting main program. Version is v{}".format(version))
    print("Input file: {}".format(input_file_path))
    print("Output file: {}".format(output_file_path))
    main()
    print("Program. Exiting.")