import pandas as pd
import logging

logger = logging.getLogger()

class Output:
    COMPANY_HEADER = "Company"
    COLUMN_1_HEADER = "S1F1"
    COLUMN_2_HEADER = "S1F2"
    COLUMN_3_HEADER = "S1F3"
    COLUMN_4_HEADER = "S1F4"
    COLUMN_5_HEADER = "S1F5"
    COLUMN_6_HEADER = "S1F6"
    COLUMN_7_HEADER = "S1F7"
    COLUMN_8_HEADER = "S1F8"
    COLUMN_9_HEADER = "F1F2"
    COLUMN_10_HEADER = "F2F3"
    COLUMN_11_HEADER = "F3F4"
    COLUMN_12_HEADER = "F4F5"
    COLUMN_13_HEADER = "F5F6"
    COLUMN_14_HEADER = "F6F7"
    COLUMN_15_HEADER = "F7F8"

    def __init__(self, output_file):
        self.output_file = output_file
        logger.info("[Output file] {}".format(self.output_file))

        self.dataframe = pd.DataFrame(
                columns=[
                    self.COMPANY_HEADER,
                    self.COLUMN_1_HEADER,
                    self.COLUMN_2_HEADER,
                    self.COLUMN_3_HEADER,
                    self.COLUMN_4_HEADER,
                    self.COLUMN_5_HEADER,
                    self.COLUMN_6_HEADER,
                    self.COLUMN_7_HEADER,
                    self.COLUMN_8_HEADER,
                    self.COLUMN_9_HEADER,
                    self.COLUMN_10_HEADER,
                    self.COLUMN_11_HEADER,
                    self.COLUMN_12_HEADER,
                    self.COLUMN_13_HEADER,
                    self.COLUMN_14_HEADER,
                    self.COLUMN_15_HEADER])
    
    def update(self, data):
        logger.info(data)
        new_dataframe = pd.DataFrame(
                [data],
                columns=[
                    self.COMPANY_HEADER,
                    self.COLUMN_1_HEADER,
                    self.COLUMN_2_HEADER,
                    self.COLUMN_3_HEADER,
                    self.COLUMN_4_HEADER,
                    self.COLUMN_5_HEADER,
                    self.COLUMN_6_HEADER,
                    self.COLUMN_7_HEADER,
                    self.COLUMN_8_HEADER,
                    self.COLUMN_9_HEADER,
                    self.COLUMN_10_HEADER,
                    self.COLUMN_11_HEADER,
                    self.COLUMN_12_HEADER,
                    self.COLUMN_13_HEADER,
                    self.COLUMN_14_HEADER,
                    self.COLUMN_15_HEADER])

        self.dataframe = self.dataframe.append(new_dataframe, ignore_index= True)

    def export(self):
        with pd.ExcelWriter(self.output_file,) as writer:
            logger.info("Exporting data to Excel file: {}".format(self.output_file))
            self.dataframe.to_excel(writer, sheet_name = "Results", index = False)