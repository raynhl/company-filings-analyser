from modules.excelfile import ExcelFile
import logging

logger = logging.getLogger()

class Input(ExcelFile):
    COMPANY_HEADER = "Company"
    S1_URL = "S-1 URL"
    F1_URL = "Filing 1"
    F2_URL = "Filing 2"
    F3_URL = "Filing 3"
    F4_URL = "Filing 4"
    F5_URL = "Filing 5"
    F6_URL = "Filing 6"
    F7_URL = "Filing 7"
    F8_URL = "Filing 8"

    def __init__(self, input_file):
        super().__init__(input_file)
        logger.info("[Input file] {}".format(input_file))
    
    def get_row(self):
        for index, row in self.dataframe.iterrows():
            yield index, [row[self.COMPANY_HEADER],
                    row[self.S1_URL],
                    row[self.F1_URL],
                    row[self.F2_URL],
                    row[self.F3_URL],
                    row[self.F4_URL],
                    row[self.F5_URL],
                    row[self.F6_URL],
                    row[self.F7_URL],
                    row[self.F8_URL],
            ]
