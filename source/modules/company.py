from modules.downloader import Downloader
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd
import logging

logger = logging.getLogger()

class Company():
    def __init__(self, name, urls):
        self.name = name
        logger.info("[Company] {}".format(self.name))

        self.urls = urls
        self.downloader = Downloader(urls)
        self.vectorizer = TfidfVectorizer(use_idf=True)
        self.word_vectors = self.vectorizer.fit_transform(self.downloader.get_documents())
        self.similarity_matrix = self.calculate_similarities()

    def calculate_similarities(self):
        return cosine_similarity(self.word_vectors, self.word_vectors)

    def get_result(self):
        result = []
        result.append(self.name)

        for index, score in enumerate(self.similarity_matrix[0][1:]):
            result.append(score)

        for index in range(1, len(self.similarity_matrix) - 1):
            result.append(self.similarity_matrix[index][index+1])

        return result

    def show_results(self, index = None):
        if index == None:
            print(self.downloader.documents[index])
        else:
            print(self.downloader.documents)

