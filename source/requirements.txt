xlrd==1.2.0
pandas==0.25.3
sklearn==0.0
openpyxl==3.0.3
beautifulsoup4==4.6.0
numpy==1.18.1
aiohttp==3.6.2
