#!/bin/bash

export DOCKER_PATH=/usr/src/filings_analysis

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input_file> <output_file>"
	exit 1
fi

if [ ! -f `pwd`/$1 ]; then
	echo "$1 is not found in `pwd`/input/"
	echo "Please refer to `pwd`/input/sample_input.xlsx for input file example."
	exit 1
fi

sudo docker run -v `pwd`/input/:$DOCKER_PATH/input -v `pwd`/output/:$DOCKER_PATH/output/ -v `pwd`/log/:$DOCKER_PATH/log/ -it --rm raynhl/filings-analyser:0.1 $1 $2